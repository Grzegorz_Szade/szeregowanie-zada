#ifndef _MACHINE_H_
#define _MACHINE_H_

#include <vector>
#include "Task.h"

class Machine
{
public:
	std::vector<Task> tasks;
	int freeTime = 0;
};

#endif //_MACHINE_H_
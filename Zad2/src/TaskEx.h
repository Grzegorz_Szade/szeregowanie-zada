#ifndef _TASKEX_H_
#define _TASKEX_H_

#include <string>
#include <vector>
#include "Task.h"

//For Algorithm
class TaskEx
{
public:
	Task actualTask;
	//not indexed by default;
	//can be from 0;
	int index = -1;
	//fro drawer
	bool hasEnded = false;
	std::vector<TaskEx *> tasksBefore;
	std::vector<TaskEx *> tasksAfter;

	TaskEx(std::string name, int duration);
};

#endif //_TASKEX_H_
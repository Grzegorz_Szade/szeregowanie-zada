#include <iostream>

#include "TaskEx.h"
#include "Machine.h"
#include "Factory.h"
#include "Algorithm.h"
#include "Drawer.h"
#include "Parser.h"
#include "ErrorHandler.h"

void printTasks(std::vector<TaskEx*>& tasks)
{
	for (int i = 0; i < tasks.size(); i++)
	{
		std::cout << tasks.at(i)->actualTask.name << " duration: " << tasks.at(i)->actualTask.duration << " index: " << tasks.at(i)->index << std::endl;
	}
	std::cout << std::endl;
}

int main()
{
	std::vector<TaskEx*> tasks;
	tasks = Parser::getTasksFromFile("dane.txt");
	Algorithm::setIndexesWithCoffmanGraham(tasks);

	std::string count_str;
	std::cout << "Podaj ilosc maszyn: ";
	std::cin >> count_str;
	int count = Parser::getNumber(count_str);
	if (count < 0)
		ErrorHandler::handleError("Error: given number must be a positive integer!");

	printTasks(tasks);
	Drawer::drawGannt(tasks, count);

	system("Pause");
	return 0;
}
#include "ErrorHandler.h"

void ErrorHandler::handleError(std::string error)
{
	std::cout << error << std::endl;
	system("Pause");
	exit(-1);
}

void ErrorHandler::handleParserError(std::string error, int line, int position)
{
	std::cout << error << " (Line: " << line << " Position: " << position << ")" << std::endl;
	system("Pause");
	exit(-1);
}

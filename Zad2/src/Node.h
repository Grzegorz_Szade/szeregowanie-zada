#ifndef _NODE_H_
#define _NODE_H_

#include <string>
#include <vector>
#include "Task.h"

//For AA visualisation and for input read - it's just node's name
class Node
{
public:
	std::string name;
};

#endif //_NODE_H_
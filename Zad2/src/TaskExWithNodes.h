#ifndef _TASKEX_WITH_NODES_H_
#define _TASKEX_WITH_NODES_H_

#include <string>
#include <vector>
#include "TaskEx.h"

//For Parser and AA diagram
struct TaskExWithNodes
{
public:
	TaskEx * taskEx;
	std::string nodeBefore;
	std::string nodeAfter;
};

#endif //_TASKEX_WITH_NODES_H_
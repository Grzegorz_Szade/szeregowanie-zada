#ifndef _FACTORY_H_
#define _FACTORY_H_

#include <vector>
#include "Machine.h"

class Factory
{
public:
	std::vector<Machine> machines;
public:
	void createNewMachine();
};

#endif //_FACTORY_H_
#include "Drawer.h"

void Drawer::drawGannt(std::vector<TaskEx*>& tasks, int availableMachines)
{
	std::vector<Machine> machines;

	//create available machines
	for (int i = 0; i < availableMachines; i++)
	{
		Machine temp;
		machines.push_back(temp);
	}

	//copy of tasks - when task is used in drawing, it is erased from this list
	std::vector<TaskEx*> tasksToDraw = tasks;

	while(!tasksToDraw.empty())
	{
		//find first free task, that is free at this moment
		int freeTaskIndex = 0;
		//Tasks are already sorted ascending
		for (int i = tasksToDraw.size() - 1; i >= 0; i--)
		{
			//check if all its before tasks has ended
			bool isFree = true; //returns true if task doesnt have any beforTasks;
			for (TaskEx* beforeTask : tasksToDraw[i]->tasksBefore)
			{
				if (!beforeTask->hasEnded)
				{
					isFree = false;
					break;
				}
			}
			if (isFree)
			{
				freeTaskIndex = i;

				break;
			}
		}

		//find the earliset machine for this task
		int earliestMachineIndex = 0;
		//initial type for checking
		for (int i = 1; i < machines.size(); i++)
		{
			if (machines[i].freeTime <= tasksToDraw[freeTaskIndex]->actualTask.startTime)
			{
				earliestMachineIndex = i;
				break;
			}
		}
		for (int i = 1; i < machines.size(); i++)
		{
			if (machines[i].freeTime <= tasksToDraw[freeTaskIndex]->actualTask.startTime)
			{
				if(tasksToDraw[freeTaskIndex]->actualTask.startTime - machines[i].freeTime < tasksToDraw[freeTaskIndex]->actualTask.startTime - machines[earliestMachineIndex].freeTime)
					earliestMachineIndex = i;
			}	
		}


		//check if an empty cell is needed
		if (tasksToDraw[freeTaskIndex]->actualTask.startTime > machines[earliestMachineIndex].freeTime)
		{
			//add empty task
			Task empty;
			empty.name = "EMPTY";
			empty.startTime = machines[earliestMachineIndex].freeTime;
			empty.duration = tasksToDraw[freeTaskIndex]->actualTask.startTime - machines[earliestMachineIndex].freeTime;

			machines[earliestMachineIndex].tasks.push_back(empty);
			machines[earliestMachineIndex].freeTime += empty.duration;
		}

		//update the starttime of the task for drawer
		tasksToDraw[freeTaskIndex]->actualTask.startTime = machines[earliestMachineIndex].freeTime;
		tasksToDraw[freeTaskIndex]->hasEnded = true;

		//set starttimes of the next tasks
		int taskEndTime = tasksToDraw[freeTaskIndex]->actualTask.startTime + tasksToDraw[freeTaskIndex]->actualTask.duration;
		for (auto task : tasksToDraw[freeTaskIndex]->tasksAfter)
		{
			if (task->actualTask.startTime < taskEndTime)
				task->actualTask.startTime = taskEndTime;
		}

		//add the free task to the machine found
		machines[earliestMachineIndex].tasks.push_back(tasksToDraw[freeTaskIndex]->actualTask);
		machines[earliestMachineIndex].freeTime += tasksToDraw[freeTaskIndex]->actualTask.duration;
		
		//delete the added task from list od the tasks to still draw
		tasksToDraw.erase(tasksToDraw.begin() + freeTaskIndex);
	}

	//calculate Cmax
	int Cmax = machines[0].freeTime;
	for (int i = 1; i < machines.size(); i++)
	{
		if (machines[i].freeTime > Cmax)
			Cmax = machines[i].freeTime;
	}

	//draw Machines

	//size between two "|"
	const int SIZE_OF_CELL = 5;
	const char FILL_CHAR = 'x';
	const char EMPTY_CHAR = ' ';

	for (int i = machines.size() - 1; i >= 0; i--)
	{
		//draw Machine
		if(machines.size() > 10 && i >= 10)
			std::cout << "M" << i << " |";
		else
			std::cout << "M " << i << " |";
		for (int j = 0; j < machines[i].tasks.size(); j++)
		{
			//draw machine's tasks
			if (machines[i].tasks[j].name != "EMPTY")
			{
				//print name
				std::cout << machines[i].tasks[j].name;
				//fill with spaces
				for (int k = 0; k < (SIZE_OF_CELL - machines[i].tasks[j].name.size()); k++)
					std::cout << FILL_CHAR;
				//fill cells with spaces by duration
				for (int k = 0; k < (machines[i].tasks[j].duration - 1) * (SIZE_OF_CELL + 1); k++)
					std::cout << FILL_CHAR;
				std::cout << "|";
			}
			else
			{
				for (int k = 0; k < (machines[i].tasks[j].duration * (SIZE_OF_CELL + 1)) - 1; k++)
					std::cout << EMPTY_CHAR;
				std::cout << "|";
			}
		}
		std::cout << std::endl;
	}

	//draw timeline
	std::cout << "   ";
	if (machines.size() > 10)
		std::cout << " ";
	for (int i = 0; i < Cmax + 1; i++)
	{
		std::cout << i;
		for (int j = 0; j < SIZE_OF_CELL - 1; j++)
			std::cout << " ";
		//double digit
		if(i < 10)
			std::cout << " ";
	}
	std::cout << std::endl;
}

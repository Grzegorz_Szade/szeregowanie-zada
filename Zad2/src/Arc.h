#ifndef _ARC_H_
#define _ARC_H_

#include <string>
#include <vector>
#include "Task.h"
#include "Node.h"

//For AA visualisation and for input read - it's the information about the arc
class Arc
{
public:
	Node startingPoint;
	Node endingPoint;

	Task task;

	std::vector<Arc*> previousArcs;
	std::vector<Arc*> nextArcs;
};

#endif //_ARC_H_
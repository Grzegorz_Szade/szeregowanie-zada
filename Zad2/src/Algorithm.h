#ifndef _ALGORITHM_H_
#define _ALGORITHM_H_

#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include <assert.h>
#include "Task.h"
#include "TaskEx.h"
#include "Node.h"

//The actual algorithm
class Algorithm
{
public:
	static void setIndexesWithCoffmanGraham(std::vector<TaskEx *>& tasks);
	static void CPM(std::vector<TaskEx*>& tasks);
	static int getCmax(std::vector<TaskEx*>& tasks);
	static bool isLowerlexicographically(TaskEx* task1, TaskEx* task2);
};

#endif //_ALGORITHM_H_
#ifndef _DRAWER_H_
#define _DRAWER_H_

#include <string>
#include <vector>
#include <iostream>
#include "Task.h"
#include "TaskEx.h"
#include "Node.h"
#include "Machine.h"

//For drawing
class Drawer
{
public:
	static void drawGannt(std::vector<TaskEx*>& tasks, int availableMachines);
};

#endif //_DRAWER_H_
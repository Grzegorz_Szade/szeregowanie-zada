#include "Algorithm.h"

void Algorithm::setL(std::vector<TaskEx *>& tasks)
{
	if (tasks.empty())
		std::cout << "No tasks given to algorithm" << std::endl;

	std::vector<TaskEx *> A;
	int indexedTasks = 0;
	std::vector<TaskEx*> sortedTasks;

	//Check for tasks without tasks after and index them
	for (int i = 0; i < tasks.size(); i++)
	{
		if (tasks.at(i)->tasksAfter.empty())
		{
			indexedTasks++;
			tasks.at(i)->index = indexedTasks - 1;
			sortedTasks.push_back(tasks.at(i));
		}
	}

	//check the rest tasks
	while(indexedTasks < tasks.size())
	{
		A.clear();

		//determining A
		for (int i = 0; i < tasks.size(); i++)
		{
			if (tasks.at(i)->index == -1)
			{
				bool belongsToA = true;
				if (tasks.at(i)->tasksAfter.empty())
				{
					belongsToA = false;
					break;
				}
				else
				{
					for (int j = 0; j < tasks.at(i)->tasksAfter.size(); j++)
					{
						if (tasks.at(i)->tasksAfter.at(j)->index == -1)
						{
							belongsToA = false;
						}
					}
				}

				if (belongsToA)
				{
					A.push_back(tasks.at(i));
				}
			}
			
		}

		//determining task with the least tasks amount after it
		if (!A.empty())
		{
			int minAmount = A.at(0)->tasksAfter.size();
			int taskToIndex = 0;
			for (int i = 1; i < A.size(); i++)
			{
				if (A.at(i)->tasksAfter.size() < minAmount)
				{
					minAmount = A.at(i)->tasksAfter.size();
					taskToIndex = i;
				}
			}

			//index the task
			indexedTasks++;
			A.at(taskToIndex)->index = indexedTasks - 1;
			sortedTasks.push_back(A.at(taskToIndex));
		}
	}
	tasks.clear();
	tasks = sortedTasks;
}

void Algorithm::CPM(std::vector<TaskEx*>& tasks)
{
	for(int i = tasks.size() - 1; i >= 0; i--)
	{
		if (tasks[i]->tasksBefore.empty())
			tasks[i]->actualTask.startTime = 0;
		else
		{
			//find the longest task
			TaskEx* pointer = tasks[i]->tasksBefore[0];
			for (int j = 1; j < tasks[i]->tasksBefore.size(); j++)
			{
				if (tasks[i]->tasksBefore[j]->actualTask.duration > pointer->actualTask.duration)
					pointer = tasks[i]->tasksBefore[j];
			}

			//add duration to the starttime
			tasks.at(i)->actualTask.startTime += pointer->actualTask.duration;

			//search for next longest tasks
			while (!pointer->tasksBefore.empty())
			{
				std::vector<TaskEx*> tasksBeforeOfPointer = pointer->tasksBefore;
				pointer = pointer->tasksBefore[0];
				for (int j = 1; j < tasksBeforeOfPointer.size(); j++)
				{
					if (tasksBeforeOfPointer[j]->actualTask.duration > pointer->actualTask.duration)
						pointer = tasksBeforeOfPointer[j];
				}
				tasks[i]->actualTask.startTime += pointer->actualTask.duration;
			}
		}
	}
}

int Algorithm::getCmax(std::vector<TaskEx*>& tasks)
{
	int ret_cMax = 0;
	for (TaskEx* task : tasks)
	{
		int taskEndTime = task->actualTask.startTime + task->actualTask.duration;
		if (taskEndTime > ret_cMax)
			ret_cMax = taskEndTime;
	}
	return ret_cMax;
}

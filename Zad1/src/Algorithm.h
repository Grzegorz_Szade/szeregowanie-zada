#ifndef _ALGORITHM_H_
#define _ALGORITHM_H_

#include <string>
#include <vector>
#include <iostream>
#include "Task.h"
#include "TaskEx.h"
#include "Node.h"

//The actual algorithm
class Algorithm
{
public:
	static void setL(std::vector<TaskEx *>& tasks);
	static void CPM(std::vector<TaskEx*>& tasks);
	static int getCmax(std::vector<TaskEx*>& tasks);
};

#endif //_ALGORITHM_H_
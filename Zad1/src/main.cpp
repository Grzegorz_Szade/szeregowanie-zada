#include <iostream>

#include "TaskEx.h"
#include "Machine.h"
#include "Factory.h"
#include "Algorithm.h"
#include "Drawer.h"
#include "Parser.h"

void printTasks(std::vector<TaskEx*>& tasks)
{
	for (int i = 0; i < tasks.size(); i++)
	{
		std::cout << tasks.at(i)->actualTask.name << " duration: " << tasks.at(i)->actualTask.duration << " startTime: " << tasks.at(i)->actualTask.startTime << std::endl;
	}
	std::cout << std::endl;
}

int main()
{
	//TaskEx* z1 = new TaskEx("Z1", 3);
	//TaskEx* z2 = new TaskEx("Z2", 8);
	//TaskEx* z3 = new TaskEx("Z3", 2);
	//TaskEx* z4 = new TaskEx("Z4", 2);
	//TaskEx* z5 = new TaskEx("Z5", 4);
	//TaskEx* z6 = new TaskEx("Z6", 6);
	//TaskEx* z7 = new TaskEx("Z7", 9);
	//TaskEx* z8 = new TaskEx("Z8", 2);
	//TaskEx* z9 = new TaskEx("Z9", 1);

	//z1->tasksAfter.push_back(z4);
	//z1->tasksAfter.push_back(z5);

	//z2->tasksAfter.push_back(z9);

	//z3->tasksAfter.push_back(z6);
	//z3->tasksAfter.push_back(z7);

	//z4->tasksBefore.push_back(z1);
	//z4->tasksAfter.push_back(z8);

	//z5->tasksBefore.push_back(z1);
	//z5->tasksAfter.push_back(z9);

	//z6->tasksBefore.push_back(z3);
	//z6->tasksAfter.push_back(z9);

	//z7->tasksBefore.push_back(z3);

	//z8->tasksBefore.push_back(z4);

	//z9->tasksBefore.push_back(z2);
	//z9->tasksBefore.push_back(z5);
	//z9->tasksBefore.push_back(z6);

	//std::vector<TaskEx*> tasks;
	//tasks.push_back(z1);
	//tasks.push_back(z2);
	//tasks.push_back(z3);
	//tasks.push_back(z4);
	//tasks.push_back(z5);
	//tasks.push_back(z6);
	//tasks.push_back(z7);
	//tasks.push_back(z8);
	//tasks.push_back(z9);

	std::vector<TaskEx*> tasks;
	std::vector< TaskExWithNodes> tasksWithNodes;

	tasks = Parser::getTasksFromFile("dane.txt", &tasksWithNodes);
	Algorithm::CPM(tasks);

	printTasks(tasks);

	Drawer::drawGannt(tasks);
	Drawer::drawAAGraph(tasks, "Graph.bmp", tasksWithNodes);

	//print cmax
	std::cout << "C max: " << Algorithm::getCmax(tasks) << std::endl;

	system("Pause");
	return 0;
}
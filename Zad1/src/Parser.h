#ifndef _PARSER_H_
#define _PARSER_H_

#include <fstream>
#include "TaskEx.h"
#include "TaskExWithNodes.h"
#include "ErrorHandler.h"

class Parser
{
private:
	//For circular reference check
	static void wasAlready(TaskEx* inThisTask, TaskEx* checkIfThisTaskAppeared, bool *result);

	//For adding references
	static bool isAlreadyReferencing(TaskEx* task, TaskEx* checkIfThisTaskAppeared);

	static std::string getNextWord(std::string line, int* currPos, int* result);
	static int getNextNumber(std::string line, int* currPos, int* result);
	static TaskExWithNodes getTaskExFromLine(std::string line, int lineNumber);

public:
	static std::vector<TaskEx*> getTasksFromFile(const char* filename, std::vector <TaskExWithNodes>* tasksWithNodes = NULL);
};

#endif //_PARSER_H_
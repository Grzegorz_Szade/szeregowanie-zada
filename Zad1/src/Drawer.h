#ifndef _DRAWER_H_
#define _DRAWER_H_

#include <string>
#include <vector>
#include <iostream>
#include "Task.h"
#include "TaskEx.h"
#include "TaskExWithNodes.h"
#include "Node.h"
#include "Machine.h"
#include "EasyBMP\EasyBMP.h"
#include "EasyBMP\EasyBMP_Geometry.h"
#include "EasyBMP\EasyBMP_Font.h"

//For drawing
class Drawer
{
public:
	static void drawGannt(std::vector<TaskEx*>& tasks);
	static void drawAAGraph(std::vector<TaskEx*>& tasks, const char* filename, std::vector<TaskExWithNodes> tasksWithNodes);
};

#endif //_DRAWER_H_
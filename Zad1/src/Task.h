#ifndef _TASK_H_
#define _TASK_H_
#include <string>

//Actual Task
typedef struct Task
{
public:
	std::string name;
	int startTime;
	int duration;

	Task() {}

	Task(std::string _name, int _startTime, int _duration)
		: name(_name),
		startTime(_startTime),
		duration(_duration)
	{
	}
};

#endif //_TASK_H_
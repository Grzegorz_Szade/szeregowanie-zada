#ifndef _TASKEX_H_
#define _TASKEX_H_

#include <string>
#include <vector>
#include "Task.h"

//For Algorithm
class TaskEx
{
public:
	Task actualTask;
	//not indexed by default;
	//can be from 0;
	int index = -1;
	bool hasBeenMarked = false;
	int row = -1;
	int xNode = -1;
	int yNode = -1;
	std::vector<TaskEx *> tasksBefore;
	std::vector<TaskEx *> tasksAfter;

	TaskEx(std::string name, int duration);
};

#endif //_TASKEX_H_
#include "Drawer.h"

struct TransferInfo
{
	Node nodeToTransfer;
	int rowDestination;
};

void drawArrow(BMP& image, int fromX, int fromY, int toX, int toY)
{
	double slopy, cosy, siny;
	double Par = 15.0;
	slopy = atan2((fromY - toY), (fromX - toX));
	cosy = cos(slopy);
	siny = sin(slopy);

	DrawAALine(image, fromX, fromY, toX, toY, { 0,0,0 });

	int toX1 = fromX + int(-Par * cosy - (Par / 2.0 * siny));
	int toY1 = fromY + int(-Par * siny + (Par / 2.0 * cosy));
	DrawAALine(image, fromX, fromY, toX1, toY1, { 0,0,0 });

	toX1 = fromX + int(-Par * cosy + (Par / 2.0 * siny));
	toY1 = fromY - int(Par / 2.0 * cosy + Par * siny);
	DrawAALine(image, fromX, fromY, toX1, toY1, { 0,0,0 });

}

Node findNode(std::string name, std::vector< std::vector<Node>> nodes)
{
	for (auto row : nodes)
	{
		for (Node n : row)
		{
			if (n.name == name)
				return n;
		}
	}

	return {};
}

int findEarliestRowIndex(std::vector<TransferInfo> t)
{
	int ret_id = 0;
	for (int i = 1; i < t.size(); i++)
	{
		if (t[i].rowDestination < t[ret_id].rowDestination)
			ret_id = i;
	}
	return ret_id;
}

std::vector<TaskExWithNodes> findTasksThatEndsWith(std::string symbol, std::vector<TaskExWithNodes> tasksWithNodes)
{
	std::vector<TaskExWithNodes> ret;
	for (auto task : tasksWithNodes)
	{
		if (task.nodeAfter == symbol)
			ret.push_back(task);
	}
	return ret;
}

std::vector <TaskExWithNodes> findTasksThatStartsWith(std::string symbol, std::vector<TaskExWithNodes> tasksWithNodes)
{
	std::vector<TaskExWithNodes> ret;
	for (auto task : tasksWithNodes)
	{
		if (task.nodeBefore == symbol)
			ret.push_back(task);
	}
	return ret;
}

int countNodes(std::vector<TaskEx*> row)
{
	int retCount = 0;
	//clear marking
	for (auto task : row)
	{
		for (auto taskAfter : task->tasksAfter)
		{
			taskAfter->hasBeenMarked = false;
		}
	}

	//count nodes
	for (auto task : row)
	{
		if (task->tasksAfter.empty())
			retCount++;
		else
		{
			for (auto taskAfter : task->tasksAfter)
			{
				if (!taskAfter->hasBeenMarked)
				{
					retCount++;
					taskAfter->hasBeenMarked = true;
					break;
				}
			}
		}
	}
	return retCount;
}

void printNode(BMP& image, const char* nodeName, int x, int y)
{
	PrintString(image, "o", x, y, 24, { 0,0,0 });
	PrintString(image, nodeName, x, y - 28, 24, { 0,0,255 });
}

void printLine(BMP& image, TaskEx* task, int xFrom, int yFrom, int xTo, int yTo)
{
	std::string str = task->actualTask.name;
	str += ":";
	str += std::to_string(task->actualTask.duration);
	drawArrow(image, xTo, yTo + 18, xFrom + 12, yFrom + 18);

	int x = xTo < xFrom ? xTo : xFrom;
	int y = yTo < yFrom ? yTo : yFrom;
	x += abs(xTo - xFrom) / 2;
	y += abs(yTo - yFrom) / 2;

	PrintString(image, str.c_str(), x, y - 28, 24, { 0,0,0 });
}

bool isIn(TaskEx* task, std::vector<TaskEx*>& tasks)
{
	for (auto t : tasks)
		if (t == task)
			return true;
	return false;
}

bool isIn(std::string nodeName, std::vector<Node> nodes)
{
	for (auto n : nodes)
		if (n.name == nodeName)
			return true;
	return false;
}

void deleteFrom(Node toDelete, std::vector<std::vector<Node>>& nodeCanvas)
{
	for (std::vector<Node>& row : nodeCanvas)
	{
		for (int i = 0; i < row.size(); i++)
		{
			if (row[i].name == toDelete.name)
			{
				row.erase(row.begin() + i);
				return;
			}
		}
	}
}

//from row 0 to lastRow
bool isInRec(std::string nodeName, std::vector<std::vector<Node>> nodes, int lastRow)
{
	for (int i = 0; i <= lastRow; i++)
		for (auto n : nodes[i])
			if (n.name == nodeName)
				return true;
	return false;
}

bool isIn(std::string nodeName, std::vector<TransferInfo> nodes)
{
	for (auto n : nodes)
		if (n.nodeToTransfer.name == nodeName)
			return true;
	return false;
}

bool isIn(std::string nodeName, std::vector<TransferInfo> nodes, TransferInfo* nodeToTransfer_dest)
{
	for (auto n : nodes)
		if (n.nodeToTransfer.name == nodeName)
		{
			*nodeToTransfer_dest = n;
			return true;
		}
	return false;
}

bool isIn(TaskExWithNodes task, std::vector<TaskExWithNodes>& tasks)
{
	for (auto t : tasks)
		if (t.taskEx == task.taskEx)
			return true;
	return false;
}

bool isThatStartsWithIn(std::string symbol, std::vector<TaskExWithNodes>& tasks)
{
	for (auto t : tasks)
		if (t.nodeBefore == symbol)
			return true;
	return false;
}

bool isThatEndsWithIn(std::string symbol, std::vector<TaskExWithNodes>& tasks)
{
	for (auto t : tasks)
		if (t.nodeAfter == symbol)
			return true;
	return false;
}

int findIndexOf(TaskEx* task, std::vector<TaskEx*>& tasks)
{
	for (int i = 0; i < tasks.size(); i++)
		if (tasks[i] == task)
			return i;
	return -1;
}

void Drawer::drawGannt(std::vector<TaskEx*>& tasks)
{
	std::vector<Machine> machines;
	Machine firstMachine;
	machines.push_back(firstMachine);

	//copy of tasks - when task is used in drawing, it is erased from this list
	std::vector<TaskEx*> tasksToDraw = tasks;

	while (!tasksToDraw.empty())
	{
		//not using index
		//find the earliest free task
		int earliestIndex = 0;
		for (int i = 1; i < tasksToDraw.size(); i++)
		{
			if (tasksToDraw[i]->actualTask.startTime < tasksToDraw[earliestIndex]->actualTask.startTime)
				earliestIndex = i;
		}

		//add the earliest task to a free machine
		bool freeMachineFound = false;
		for (int i = 0; i < machines.size(); i++)
		{
			if (tasksToDraw[earliestIndex]->actualTask.startTime == machines[i].freeTime)
			{
				freeMachineFound = true;
				machines[i].freeTime += tasksToDraw[earliestIndex]->actualTask.duration;
				machines[i].tasks.push_back(tasksToDraw[earliestIndex]->actualTask);
				break;
			}
		}
		if (!freeMachineFound)
		{
			for (int i = 0; i < machines.size(); i++)
			{
				if (tasksToDraw[earliestIndex]->actualTask.startTime > machines[i].freeTime)
				{
					//add empty task And the actual task
					Task empty;
					empty.name = "EMPTY";
					empty.startTime = machines[i].freeTime;
					empty.duration = tasksToDraw[earliestIndex]->actualTask.startTime - machines[i].freeTime;

					freeMachineFound = true;
					machines[i].freeTime += empty.duration;
					machines[i].freeTime += tasksToDraw[earliestIndex]->actualTask.duration;
					machines[i].tasks.push_back(empty);
					machines[i].tasks.push_back(tasksToDraw[earliestIndex]->actualTask);
					break;
				}
			}
		}
		//create new machine if free is not found
		if (!freeMachineFound)
		{
			Machine m;
			m.freeTime = 0;
			if (tasksToDraw[earliestIndex]->actualTask.startTime == 0)
			{
				m.freeTime += tasksToDraw[earliestIndex]->actualTask.duration;
				m.tasks.push_back(tasksToDraw[earliestIndex]->actualTask);
			}
			else
			{
				Task empty;
				empty.name = "EMPTY";
				empty.startTime = 0;
				empty.duration = tasksToDraw[earliestIndex]->actualTask.startTime;
				m.freeTime += empty.duration;
				m.freeTime += tasksToDraw[earliestIndex]->actualTask.duration;
				m.tasks.push_back(empty);
				m.tasks.push_back(tasksToDraw[earliestIndex]->actualTask);
			}
			machines.push_back(m);
		}

		//delete added task from list
		tasksToDraw.erase(tasksToDraw.begin() + earliestIndex);
	}

	//calculate Cmax
	int Cmax = machines[0].freeTime;
	for (int i = 1; i < machines.size(); i++)
	{
		if (machines[i].freeTime > Cmax)
			Cmax = machines[i].freeTime;
	}

	//draw Machines

	//size between two "|"
	const int SIZE_OF_CELL = 5;
	const char FILL_CHAR = 'x';
	const char EMPTY_CHAR = ' ';

	for (int i = machines.size() - 1; i >= 0; i--)
	{
		//draw Machine
		std::cout << "M" << i << " |";
		for (int j = 0; j < machines[i].tasks.size(); j++)
		{
			//draw machine's tasks
			if (machines[i].tasks[j].name != "EMPTY")
			{
				//print name
				std::cout << machines[i].tasks[j].name;
				//fill with spaces
				for (int k = 0; k < (SIZE_OF_CELL - machines[i].tasks[j].name.size()); k++)
					std::cout << FILL_CHAR;
				//fill cells with spaces by duration
				for (int k = 0; k < (machines[i].tasks[j].duration - 1) * (SIZE_OF_CELL + 1); k++)
					std::cout << FILL_CHAR;
				std::cout << "|";
			}
			else
			{
				for (int k = 0; k < (machines[i].tasks[j].duration * (SIZE_OF_CELL + 1)) - 1; k++)
					std::cout << EMPTY_CHAR;
				std::cout << "|";
			}
		}
		std::cout << std::endl;
	}

	//draw timeline
	std::cout << "   ";
	for (int i = 0; i < Cmax + 1; i++)
	{
		std::cout << i;
		for (int j = 0; j < SIZE_OF_CELL - 1; j++)
			std::cout << " ";
		//double digit
		if (i < 10)
			std::cout << " ";
	}
	std::cout << std::endl;
}

void Drawer::drawAAGraph(std::vector<TaskEx*>& tasks, const char* filename, std::vector<TaskExWithNodes> tasksWithNodes)
{
	//place the tasks in node canvas
	std::vector<std::vector<Node>> nodeCanvas; //nodeCanvas[columnnr][node]

	//create first row
	nodeCanvas.push_back(std::vector<Node>());
	nodeCanvas[0].push_back({ "Z" });

	//create next row
	nodeCanvas.push_back(std::vector<Node>());

	auto startingNodes = findTasksThatStartsWith("Z", tasksWithNodes);
	for (auto node : startingNodes)
	{
		nodeCanvas[1].push_back({ node.nodeAfter });
	}

	int row = 1;

	bool empty = false;
	while (!empty)
	{
		empty = true;

		//create next row
		nodeCanvas.push_back(std::vector<Node>());

		auto nodes = nodeCanvas[row];
		for (auto node : nodes)
		{
			for (auto nodeTask : findTasksThatStartsWith(node.name, tasksWithNodes))
			{
				empty = false;
				//check if already is there
				if (!isIn(nodeTask.nodeAfter, nodeCanvas[row]))
				{
					if (!isIn(nodeTask.nodeAfter, nodeCanvas[row + 1]))
						nodeCanvas[row + 1].push_back({ nodeTask.nodeAfter });

				}
				else
					for (auto& n : nodeCanvas)
					{
						for (auto& n2 : n)
						{
							if (n2.name == nodeTask.nodeAfter)
								n2.marked = true;
						}
					}


			}
		}
		row++;
	}

	//Determine max height in tasks
	int maxHeight = 0;
	for (auto column : nodeCanvas)
	{
		if (column.size() > maxHeight)
			maxHeight = column.size();
	}

	//Determine max width in tasks
	int maxWidth = nodeCanvas.size();

	//draw on actual bmp file

	//background color
	RGBApixel bgColor;
	bgColor.Red = 173;
	bgColor.Green = 216;
	bgColor.Blue = 230;

	RGBApixel fontColor;
	fontColor.Red = 255;
	fontColor.Green = 0;
	fontColor.Blue = 0;
	int fontSize = 24;

	BMP image;
	int space = 64;
	int width = space + (space * 6 * (maxWidth + 1)) - (space * 2);
	int height = space * 3 * maxWidth;
	image.SetSize(width, height);
	for (int j = 0; j < image.TellHeight(); j++)
	{
		for (int i = 0; i < image.TellWidth(); i++)
		{
			*image(i, j) = bgColor;
		}
	}


	row = 0;
	//first row
	for (int i = 0; i < nodeCanvas[row].size(); i++)
	{
		nodeCanvas[row][i].x = 64 + ((row) * 6 * 64);
		nodeCanvas[row][i].y = ((image.TellHeight() / (nodeCanvas[row].size() + 1)) * (i + 1));
		if (nodeCanvas[row][i].marked)
			nodeCanvas[row][i].x += 3 * 64;
		printNode(image, nodeCanvas[row][i].name.c_str(), nodeCanvas[row][i].x, nodeCanvas[row][i].y);
	}

	//print the rest
	for (row = 1; row < nodeCanvas.size(); row++)
	{
		for (int j = 0; j < nodeCanvas[row].size(); j++)
		{
			nodeCanvas[row][j].x = 64 + ((row) * 6 * 64);
			nodeCanvas[row][j].y = ((image.TellHeight() / (nodeCanvas[row].size() + 1)) * (j + 1));
			if (nodeCanvas[row][j].marked)
				nodeCanvas[row][j].x += 3 * 64;
			if (nodeCanvas[row][j].name == "U")
				nodeCanvas[row][j].x += 3 * 64;
			printNode(image, nodeCanvas[row][j].name.c_str(), nodeCanvas[row][j].x, nodeCanvas[row][j].y);
		}
	}

	//print lines
	for (auto task : tasksWithNodes)
	{
		Node startingNode = findNode(task.nodeBefore, nodeCanvas);
		Node endingNode = findNode(task.nodeAfter, nodeCanvas);
		printLine(image, task.taskEx, startingNode.x, startingNode.y, endingNode.x, endingNode.y);
	}

	image.WriteToFile(filename);
}

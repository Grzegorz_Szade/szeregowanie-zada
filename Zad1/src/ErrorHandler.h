#ifndef _ERROR_HANDLER_H_
#define _ERROR_HANDLER_H_

#include <string>
#include <vector>
#include <iostream>
#include "Task.h"
#include "TaskEx.h"
#include "Node.h"

//The actual algorithm
class ErrorHandler
{
public:
	static void handleError(std::string error);
	static void handleParserError(std::string error, int line, int position);
};

#endif //_ERROR_HANDLER_H_
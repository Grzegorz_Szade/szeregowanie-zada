#include "Parser.h"

std::string Parser::getNextWord(std::string line, int * currPos, int * result)
{
	std::size_t spacePos = line.find(" ", *currPos);
	if (spacePos == std::string::npos)
		*result = -1;
	std::string ret_str = line.substr(*currPos, spacePos - *currPos);
	*currPos = spacePos + 1;
	*result = 0;
	return ret_str;
}

int Parser::getNextNumber(std::string line, int* currPos, int* result)
{
	std::size_t endLinePos = line.find("\n", *currPos);
	if (endLinePos == std::string::npos)
		*result = -2;
	std::string ret_str = line.substr(*currPos, endLinePos - *currPos);
	*currPos = endLinePos + 1;
	int ret_number;
	try
	{
		ret_number = std::stoi(ret_str);
	}
	catch (std::invalid_argument const &e)
	{
		*result = -1;
		return 0;
	}
	catch (std::out_of_range const &e)
	{
		*result = -1;
		return 0;
	}

	if (ret_number < 0)
		*result = -1;

	*result = 0;
	return ret_number;
}

TaskExWithNodes Parser::getTaskExFromLine(std::string line, int lineNumber)
{
	//Info to store
	std::string nodeBefore;
	std::string name;
	std::string nodeAfter;
	int duration;

	int currPos = 0;
	int res = 0;
	//Start node
	nodeBefore = getNextWord(line, &currPos, &res);
	if (res == -1)
		ErrorHandler::handleParserError("Error: Expected node name", lineNumber, currPos);
	if (nodeBefore == "U")
		ErrorHandler::handleParserError("Error: Starting node can't be the end node (end node is marked as U)", lineNumber, currPos);

	//Name
	name = getNextWord(line, &currPos, &res);
	if (res == -1)
		ErrorHandler::handleParserError("Error: Expected task name", lineNumber, currPos);

	//End node
	nodeAfter = getNextWord(line, &currPos, &res);
	if (res == -1)
		ErrorHandler::handleParserError("Error: Expected node name", lineNumber, currPos);
	if (nodeAfter == "Z")
		ErrorHandler::handleParserError("Error: Ending node can't be the Start node (start node is marked as Z)", lineNumber, currPos);

	//Duration
	duration = getNextNumber(line, &currPos, &res);
	if (res == -1)
		ErrorHandler::handleParserError("Error: Expected a number", lineNumber, currPos);
	if (res == -2)
		ErrorHandler::handleParserError("Error: Expected an end of line", lineNumber, currPos);
	TaskEx* taskEx = new TaskEx(name, duration);
	return { taskEx, nodeBefore, nodeAfter };
}

void Parser::wasAlready(TaskEx* inThisTask, TaskEx* checkIfThisTaskAppeared, bool * result)
{
	if (checkIfThisTaskAppeared == NULL)
		return;
	if (inThisTask->tasksBefore.size() == 0)
		return;
	for (TaskEx* taskBefore : inThisTask->tasksBefore)
	{
		if (taskBefore == checkIfThisTaskAppeared)
		{
			*result = true;
			return;
		}
		wasAlready(taskBefore, checkIfThisTaskAppeared, result);
	}
}

bool Parser::isAlreadyReferencing(TaskEx* task, TaskEx* checkIfThisTaskAppeared)
{
	for (TaskEx* taskAfter : task->tasksAfter)
	{
		if (taskAfter == checkIfThisTaskAppeared)
			return true;
	}
	return false;
}

std::vector<TaskEx *> Parser::getTasksFromFile(const char* filename, std::vector <TaskExWithNodes>* tasksWithNodes)
{
	std::fstream file;
	std::vector<TaskEx *> ret_vect;
	std::vector<TaskExWithNodes> nodesVect;
	file.open(filename, std::ios::in);
	if (!file.good())
	{
		ErrorHandler::handleError("Error: Couldn't open the file");
	}

	//Load the data to the vectors
	std::string line;
	int lineCount = 0;
	while (getline(file, line))
	{
		//load to task structure
		TaskExWithNodes taskExWithNodes = getTaskExFromLine(line, lineCount);
		ret_vect.push_back(taskExWithNodes.taskEx);
		nodesVect.push_back(taskExWithNodes);

		lineCount++;
	}

	//Add references
	for (TaskExWithNodes& task1 : nodesVect)
	{
		for (TaskExWithNodes& task2 : nodesVect)
		{
			//continue if checks itself
			if (task1.taskEx != task2.taskEx)
			{
				//Check after node
				if (task1.nodeAfter == task2.nodeBefore)
				{
					bool res = isAlreadyReferencing(task1.taskEx, task2.taskEx);
					if (!res)
					{
						task1.taskEx->tasksAfter.push_back(task2.taskEx);
						task2.taskEx->tasksBefore.push_back(task1.taskEx);
					}
				}
			}
		}
	}

	//Check for circular references
	for (TaskEx* taskEx : ret_vect)
	{
		for (int i = 0; i < taskEx->tasksAfter.size(); i++)
		{
			bool result;
			wasAlready(taskEx, taskEx->tasksAfter[i], &result);
			if(result == true)
				ErrorHandler::handleError("Error: Wykryto zaleznosc cykliczna");
		}
	}

	//Cleanup and return values
	file.close();

	if (tasksWithNodes != NULL)
		*tasksWithNodes = nodesVect;

	return ret_vect;
}
